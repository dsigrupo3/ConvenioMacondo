﻿<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Heredamos de la clase Private_Controller */
class Tipo_Convenio extends Private_Controller {
 
  function __construct()
  {
 
    parent::__construct();
 
    /* Cargamos la base de datos */
    $this->load->database();
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
 
    /* Añadimos el helper al controlador */
    $this->load->helper('url');
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    if(!@$this->user) redirect ('welcome/login');

    redirect('Tipo_Convenio/administracion');
  }
 
  /*
   *
   **/
  function administracion()
  {
    if(!@$this->user) redirect ('welcome/login');

    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();
 
 
    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('cnv_tipo_convenio');
 
    /* Le asignamos un nombre */
    $crud->set_subject('Administración de Tipo de Convenio');
 
    /* Asignamos el idioma español */
    $crud->set_language('spanish');
 
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'ID_TIPO_CONVENIO',
      'NOMBRE_TIPO_CONVENIO',
      'NEMONICO',
      'VIGENTE'
    );
 
    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'ID_TIPO_CONVENIO',
      'NOMBRE_TIPO_CONVENIO',
      'NEMONICO',
      'VIGENTE'
    );

    /* Aqui se puede modificar el nombre de los campos en la pantalla */
    $crud->display_as('ID_TIPO_CONVENIO','Identificador')
         ->display_as('NOMBRE_TIPO_CONVENIO','Nombre Convenio')
         ->display_as('NEMONICO','Nemonico')
         ->display_as('VIGENTE','Vigencia');
 
    /* Generamos la tabla */
    $output = $crud->render();

 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
    
    $this->load->view('header', $output);
    $this->load->view('sidebar');
    $this->load->view('tipo_convenio/administracion', $output);
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  }
}