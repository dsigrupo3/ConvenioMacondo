<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('america/santiago');

/* Heredamos de la clase Private_Controller */
class coordinador_convenio extends Private_Controller {
 
  function __construct()
  {
 
    parent::__construct();
 
    /* Cargamos la base de datos */
    $this->load->database();
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
 
    /* Añadimos el helper al controlador */
    $this->load->helper('url');
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    if(!@$this->user) redirect ('welcome/login');

    redirect('coordinador_convenio/administracion');
  }

  /*
   *
   **/
  function administracion()
  {
    if(!@$this->user) redirect ('welcome/login');

    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();

    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('cnv_coordinador_convenio');
 
    /* Le asignamos un nombre */
    $crud->set_subject('Administración de Coordinador de Convenios');
 
    /* Asignamos el idioma español */
    $crud->set_language('spanish');

    /* Se generan las validaciones para los campos */
    $crud->set_rules('RUT_COORDINADOR_CONVENIO', 'RUT Coordinador', 'callback_rut_check');
    $crud->set_rules('EMAIL','E-mail','required|valid_email');
 
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'ID_INSTITUCION',
      'RUT_COORDINADOR_CONVENIO',
      'NOMBRE_COORDINADOR_CONVENIO',
      'FECHA_INICIO',
      'FECHA_FIN',
      'VIGENTE',
      'ESEXTERNO',
      'UNIDAD_ACADEMICA',
      'EMAIL'
    );
 
    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'ID_COORDINADOR_CONVENIO',
      'ID_INSTITUCION',
      'RUT_COORDINADOR_CONVENIO',
      'NOMBRE_COORDINADOR_CONVENIO',
      'FECHA_INICIO',
      'FECHA_FIN',
      'VIGENTE',
      'ESEXTERNO',
      'UNIDAD_ACADEMICA',
      'EMAIL'
    );

    $crud->set_relation('ID_INSTITUCION','cnv_institucion','NOMBRE_INSTITUCION');

    /* Aqui se puede modificar el nombre de los campos en la pantalla */
    $crud->display_as('ID_COORDINADOR_CONVENIO','ID Coordinador')
         ->display_as('ID_INSTITUCION','Nombre Institución')
         ->display_as('RUT_COORDINADOR_CONVENIO','RUT Coordinador')
         ->display_as('NOMBRE_COORDINADOR_CONVENIO','Nombre Coordinador')
         ->display_as('FECHA_INICIO','Fecha Inicio')
         ->display_as('FECHA_FIN','Fin')
         ->display_as('VIGENTE','Vigencia')
         ->display_as('ESEXTERNO','Externo')
         ->display_as('UNIDAD_ACADEMICA','Unidad Académica')
         ->display_as('EMAIL','E-mail');   
 
    /* Generamos la tabla */
    $output = $crud->render();

    
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
    $this->load->view('header', $output);
    $this->load->view('sidebar');
    $this->load->view('coordinador_convenio/administracion', $output);
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  }

  function rut_check($rut)
    {
        $this->form_validation->set_message('rut_check', 'El campo {field} es inválido');
        if ($rut == ''){
            $this->form_validation->set_message('rut_check', 'El campo {field} es obligatorio');
            return FALSE;
        }
        else
        {
            $rut= str_replace('.', '', $rut); 
            if(strlen($rut) > 10) 
            { 
                return false; 
            } 
            
            if(strstr($rut, '-') == false) 
            { 
                return false; 
            } 
            
            $array_rut_sin_guion = explode('-',$rut); // separamos el la cadena del digito verificador. 
            $rut_sin_guion = $array_rut_sin_guion[0]; // la primera cadena 
            $digito_verificador = $array_rut_sin_guion[1];// el digito despues del guion. 


            if(is_numeric($rut_sin_guion)== false) 
            { 
                return false; 
            } 
            if ($digito_verificador != 'k' and $digito_verificador != 'K') 
            { 
                if(is_numeric($digito_verificador)== false)  
                  { 
                    return false; 
                  } 
            } 
            $cantidad = strlen($rut_sin_guion); //8 o 7 elementos 
            for ( $i = 0; $i < $cantidad; $i++)//pasamos el rut sin guion a un vector 
            { 
                $rut_array[$i] = $rut_sin_guion{$i}; 
            }   

            $i = ($cantidad-1); 
            $x=$i; 
            for ($ib = 0; $ib < $cantidad; $ib++)// ingresamos los elementos del ventor rut_array en otro vector pero al reves. 
            { 
                $rut_reverse[$ib]= $rut_array[$i];      
                $rut_reverse[$ib]; 
                $i=$i-1; 
            } 
                 
            $i=2; 
            $ib=0; 
            $acum=0;  
            do 
            { 
                if( $i > 7 ) 
                { 
                    $i=2; 
                } 
            $acum = $acum + ($rut_reverse[$ib]*$i); 
            $i++; 
            $ib++; 
            }while ( $ib <= $x); 

            $resto = $acum%11; 
            $resultado = 11-$resto; 
            if ($resultado == 11) { $resultado=0; } 
            if ($resultado == 10) { $resultado='k'; } 
            if ($digito_verificador == 'k' or $digito_verificador =='K') { $digito_verificador='k';} 

            if ($resultado == $digito_verificador) 
            { 
                return true; 
            } 
            else 
            { 
                return false; 
            } 

        }
    }

}