<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/* Heredamos de la clase Private_Controller */
class Modalidad_colaboracion extends Private_Controller {
 
  function __construct()
  {
 
    parent::__construct();
 
    /* Cargamos la base de datos */
    $this->load->database();
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
 
    /* Añadimos el helper al controlador */
    $this->load->helper('url');
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    if(!@$this->user) redirect ('welcome/login');

    redirect('modalidad_colaboracion/administracion');
  }
 
  /*
   *
   **/
  function administracion()
  {
    if(!@$this->user) redirect ('welcome/login');

    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();
 
    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('cnv_modalidad_colaboracion');
 
    /* Le asignamos un nombre */
    $crud->set_subject('Administración de Modalidad Colaboración');
 
    /* Asignamos el idioma español */
    $crud->set_language('spanish');
 
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'ID_MODALIDAD_COLABORACION',
      'NOMBRE_MODALIDAD_COLABORACION',
      'DESCRIPCION',
      'VIGENTE'
      
    );
 
    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'ID_MODALIDAD_COLABORACION',
      'NOMBRE_MODALIDAD_COLABORACION',
      'DESCRIPCION',
      'VIGENTE'
    );
 
   $crud->display_as('ID_MODALIDAD_COLABORACION','ID')
        ->display_as('NOMBRE_MODALIDAD_COLABORACION','Nombre Modalidad Colaboración')
        ->display_as('DESCRIPCION','Descripción')
        ->display_as('VIGENTE','Vigente');

    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/modalidad_colaboracion/administracion.php */
    $this->load->view('header', $output);
    $this->load->view('sidebar');
    $this->load->view('modalidad_colaboracion/administracion', $output);
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  }
}