<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/* Heredamos de la clase Private_Controller */
class Estado_convenio extends Private_Controller {
 
  function __construct()
  {
 
    parent::__construct();
 
    /* Cargamos la base de datos */
    $this->load->database();
 
    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');
 
    /* Añadimos el helper al controlador */
    $this->load->helper('url');
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    if(!@$this->user) redirect ('welcome/login');

    redirect('estado_convenio/administracion');
  }
 
  /*
   *
   **/
  function administracion()
  {
    if(!@$this->user) redirect ('welcome/login');

    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();
 
    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('cnv_estado_convenio');
 
    /* Le asignamos un nombre */
    $crud->set_subject('Administración de Estados de Convenios');
 
    /* Asignamos el idioma español */
    $crud->set_language('spanish');
 
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'ID_ESTADO_CONVENIO',
      'NOMBRE_ESTADO_CONVENIO',
      'DESCRIPCION',
      'VIGENTE'
      
    );
 
    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'ID_ESTADO_CONVENIO',
      'NOMBRE_ESTADO_CONVENIO',
      'DESCRIPCION',
      'VIGENTE'
    );
    
    $crud->display_as('ID_ESTADO_CONVENIO','ID')
         ->display_as('NOMBRE_ESTADO_CONVENIO','Nombre Estado Convenio')
         ->display_as('DESCRIPCION','Descripción')
         ->display_as('VIGENTE','Vigente');

    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/estado_convenio/administracion.php */
    $this->load->view('header', $output);
    $this->load->view('sidebar');
    $this->load->view('estado_convenio/administracion', $output);
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  }
}