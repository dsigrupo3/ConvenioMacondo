<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Convenios extends Private_Controller {
    
    public function index()
    {
        if(!@$this->user) redirect ('welcome/login');
   
        redirect('convenios/administracion');
    }

	public function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('grocery_crud');
	}

	public function administracion(){
        if(!@$this->user) redirect ('welcome/login');

        try{
            $crud = new grocery_CRUD();
            
            $crud->set_table('cnv_convenio');
            $crud->set_subject('Administración de Convenios');
            $crud->set_language('spanish');
    
            $crud->required_fields(
                'ID_TIPO_CONVENIO',
                'ID_ESTADO_CONVENIO',
                'NOMBRE_CONVENIO',
                'FECHA_INICIO',
                'FECHA_FIRMA',
                'FECHA_DECRETO',
                'NUMERO_DECRETO'
            );

            $crud->columns(
                'ID_CONVENIO',
                'ID_TIPO_CONVENIO',
                'ID_COORDINADOR_CONVENIO',
                'ID_ESTADO_CONVENIO',
                'NOMBRE_CONVENIO',
                'FECHA_INICIO',
                'FECHA_FIRMA',
                'FECHA_DECRETO',
                'NUMERO_DECRETO',
                'DESCRIPCION',
                'VIGENTE',
                'VIGENCIA'
            );

            /* Relaciones de la tabla convenios */
            
            /* Relaciones n a n */
            $crud->set_relation_n_n(
                'Institucion', 
                'cnv_convenio_institucion', 
                'cnv_institucion', 
                'ID_CONVENIO', 
                'ID_INSTITUCION',
                'NOMBRE_INSTITUCION'
            );

            $crud->set_relation_n_n(
                'Modalidad Colaboracion', 
                'cnv_modalidad_colaboracion_convenio', 
                'cnv_modalidad_colaboracion', 
                'ID_CONVENIO', 
                'ID_MODALIDAD_COLABORACION',
                'NOMBRE_MODALIDAD_COLABORACION'
            );
            
            /* Relaciones 1 a n */
            $crud->set_relation('ID_TIPO_CONVENIO','cnv_tipo_convenio','NOMBRE_TIPO_CONVENIO');
            $crud->set_relation('ID_COORDINADOR_CONVENIO','cnv_coordinador_convenio','NOMBRE_COORDINADOR_CONVENIO');
            $crud->set_relation('ID_ESTADO_CONVENIO','cnv_estado_convenio','NOMBRE_ESTADO_CONVENIO');

            /* Nombre de los campos */
            $crud->display_as('ID_ESTADO_CONVENIO','Estado del Convenio')
                ->display_as('ID_COORDINADOR_CONVENIO','Coordinador del Convenio')
                ->display_as('ID_TIPO_CONVENIO','Tipo de Convenio')
                ->display_as('NOMBRE_CONVENIO','Nombre')
                ->display_as('FECHA_INICIO','Fecha de Inicio')
                ->display_as('FECHA_TERMINO','Fecha de Termino')
                ->display_as('FECHA_FIRMA','Fecha de Firma')
                ->display_as('FECHA_DECRETO','Fecha de Decreto')
                ->display_as('NUMERO_DECRETO','Numero del Decreto')
                ->display_as('DESCRIPCION','Descripcion')
                ->display_as('VIGENTE','Vigente')
                ->display_as('VIGENCIA','Vigencia');

            $output = $crud->render();    
            $this->load->view('header', $output);
            $this->load->view('sidebar');
            $this->load->view('convenios/administracion', $output);    
        
        }catch(Exception $e){
        show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

}