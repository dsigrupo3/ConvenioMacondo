<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('america/santiago');

/* Heredamos de la clase Private_Controller */
class Actividades extends Private_Controller {

  function __construct()
  {

    parent::__construct();

    /* Cargamos la base de datos */
    $this->load->database();

    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');

    /* Añadimos el helper al controlador */
    $this->load->helper('url');
  }

  function index()
  {
        if(!@$this->user) redirect ('welcome/login');

    redirect('Actividades/administracion');
  }

  /*
   *
   **/
  function administracion()
  {
     if(!@$this->user) redirect ('welcome/login');
    try{

    /* Creamos el objeto */
    $crud = new grocery_CRUD();

    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('cnv_actividad_convenio');

    /* Le asignamos un nombre */
    $crud->set_subject('Administración de Actividades de Convenios');

    /* Asignamos el idioma español */
    $crud->set_language('spanish');

    /*Crear la relación con la clave foranea de la tabla convenios*/
    $crud->set_relation('ID_CONVENIO','cnv_convenio','NOMBRE_CONVENIO');

    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'ID_ESTADO_ACTIVIDAD',
      'ID_TIPO_ACTIVIDAD',
      'ID_RESPONSABLE_ACTIVIDAD',
      'ID_ACTIVIDAD_CONVENIO',
      'ID_CONVENIO',
      'NOMBRE_ACTIVIDAD',
      'DESCRIPCION',
      'FECHA_INICIO',
      'FECHA_FIN',
      'VIGENTE'
    );

    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'ID_ESTADO_ACTIVIDAD',
      'ID_TIPO_ACTIVIDAD',
      'ID_RESPONSABLE_ACTIVIDAD',
      'ID_ACTIVIDAD_CONVENIO',
      'ID_CONVENIO',
      'NOMBRE_ACTIVIDAD',
      'DESCRIPCION',
      'FECHA_INICIO',
      'FECHA_FIN',
      'VIGENTE'
    );

    /* Aqui le indicamos el orden en el que deseamos mostrar */
    $crud->edit_fields(
      'ID_ESTADO_ACTIVIDAD',
      'ID_TIPO_ACTIVIDAD',
      'ID_RESPONSABLE_ACTIVIDAD',
      'ID_ACTIVIDAD_CONVENIO',
      'ID_CONVENIO',
      'NOMBRE_ACTIVIDAD',
      'DESCRIPCION',
      'FECHA_INICIO',
      'FECHA_FIN',
      'VIGENTE'
    );

    $crud->add_fields(
      'ID_ESTADO_ACTIVIDAD',
      'ID_TIPO_ACTIVIDAD',
      'ID_RESPONSABLE_ACTIVIDAD',
      'ID_ACTIVIDAD_CONVENIO',
      'ID_CONVENIO',
      'NOMBRE_ACTIVIDAD',
      'DESCRIPCION',
      'FECHA_INICIO',
      'FECHA_FIN',
      'VIGENTE'
    );


     /* Aqui se puede modificar el nombre de los campos en la pantalla */
    $crud->display_as('ID_ESTADO_ACTIVIDAD','ID Estado')
         ->display_as('ID_TIPO_ACTIVIDAD','ID Tipo')
         ->display_as('ID_RESPONSABLE_ACTIVIDAD','ID Encargado')
         ->display_as('FECHA_INICIO','Inicio')
         ->display_as('FECHA_FIN','Fin')
         ->display_as('ID_ACTIVIDAD_CONVENIO','ID Actividad')
         ->display_as('ID_CONVENIO','Nombre Convenio')
         ->display_as('NOMBRE_ACTIVIDAD','Nombre')
         ->display_as('DESCRIPCION','Descripción')
         ->display_as('VIGENTE','Vigente');


    /* Generamos la tabla */
    $output = $crud->render();
    $this->load->view('header', $output);
    $this->load->view('sidebar');
    $this->load->view('actividades/administracion', $output);

    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  }
}