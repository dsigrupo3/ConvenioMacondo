<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('america/santiago');

/* Heredamos de la clase Private_Controller */
class Tipos_usuario extends Private_Controller {

  function __construct()
  {

    parent::__construct();

    /* Cargamos la base de datos */
    $this->load->database();

    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');

    /* Añadimos el helper al controlador */
    $this->load->helper('url');
  }

  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    if(!@$this->user) redirect ('welcome/login');

    redirect('tipos_usuario/administracion');
  }

  /*
   *
   **/
  function administracion()
  {
    if(!@$this->user) redirect ('welcome/login');

    try{

    /* Creamos el objeto */
    $crud = new grocery_CRUD();

    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('tipos_usuario');

    /* Le asignamos un nombre */
    $crud->set_subject('Administración de Tipo de Usuarios');

    /* Asignamos el idioma español */
    $crud->set_language('spanish');

    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'ID_TIPO',
      'NOMBRE',
      'DESCRIPCION'
    );

    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'ID_TIPO',
      'NOMBRE',
      'DESCRIPCION'
    );

     /* Aqui se puede modificar el nombre de los campos en la pantalla */
    $crud->display_as('ID_TIPO','ID Tipo')
         ->display_as('NOMBRE', 'Nombre del Tipo')
         ->display_as('DESCRIPCION','Descripción');


    /* Generamos la tabla */
    $output = $crud->render();
    $this->load->view('header', $output);
    $this->load->view('sidebar');
    $this->load->view('usuarios/administracion', $output);

    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  }
}