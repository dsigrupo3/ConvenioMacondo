<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('america/santiago');

/* Heredamos de la clase Private_Controller */
class Usuarios extends Private_Controller {

  function __construct()
  {

    parent::__construct();

    /* Cargamos la base de datos */
    $this->load->database();

    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');

    /* Añadimos el helper al controlador */
    $this->load->helper('url');
  }

  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    if(!@$this->user) redirect ('welcome/login');

    redirect('usuarios/administracion');
  }

  /*
   *
   **/
  function administracion()
  {
    if(!@$this->user) redirect ('welcome/login');

    try{

    /* Creamos el objeto */
    $crud = new grocery_CRUD();

    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('usuario');

    /* Le asignamos un nombre */
    $crud->set_subject('Administración de Usuarios');

    /* Asignamos el idioma español */
    $crud->set_language('spanish');

    /*Crear la relación con la clave foranea de la tabla convenios*/
    $crud->set_relation('ID_TIPO','tipos_usuario','NOMBRE');

    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'ID_USUARIO',
      'ID_TIPO',
      'USERNAME',
      'EMAIL',
      'PASSWORD'
    );

    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'ID_USUARIO',
      'ID_TIPO',
      'USERNAME',
      'EMAIL',
      'PASSWORD'
    );

    /* Aqui le indicamos el orden en el que deseamos mostrar */
    $crud->edit_fields(
      'ID_USUARIO',
      'ID_TIPO',
      'USERNAME',
      'PASSWORD',
      'EMAIL'
    );

    $crud->add_fields(
      'ID_USUARIO',
      'ID_TIPO',
      'USERNAME',
      'PASSWORD',
      'EMAIL'
    );


     /* Aqui se puede modificar el nombre de los campos en la pantalla */
    $crud->display_as('ID_USUARIO','ID Usuario')
         ->display_as('ID_TIPO', 'Tipo de Usuario')
         ->display_as('USERNAME','Usuario')
         ->display_as('PASSWORD','Contraseña')
         ->display_as('EMAIL','E-Mail');


    /* Generamos la tabla */
    $output = $crud->render();
    $this->load->view('header', $output);
    $this->load->view('sidebar');
    $this->load->view('usuarios/administracion', $output);

    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  }
}