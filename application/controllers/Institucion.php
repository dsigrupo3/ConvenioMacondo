<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Heredamos de la clase Private_Controller */
class Institucion extends Private_Controller {

  function __construct()
  {

    parent::__construct();

    /* Cargamos la base de datos */
    $this->load->database();

    /* Cargamos la libreria*/
    $this->load->library('grocery_crud');

    /* Añadimos el helper al controlador */
    $this->load->helper('url');
  }

  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    if(!@$this->user) redirect ('welcome/login');

    redirect('institucion/administracion');
  }

  /*
   *
   **/
  function administracion()
  {
    if(!@$this->user) redirect ('welcome/login');

    try{

    /* Creamos el objeto */
    $crud = new grocery_CRUD();

    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('cnv_institucion');

    /* Le asignamos un nombre */
    $crud->set_subject('Administración de Instituciones');

    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->set_rules('EMAIL_INSTITUCION','Correo','required|valid_email');
    $crud->set_rules('RUT_INSTITUCION', 'RUT Institución', 'callback_rut_check');

    $crud->required_fields(
      'ID_INSTITUCION',
      'NOMBRE_INSTITUCION',
      'ID_PAIS',
      'ID_TIPO_INSTITUCION',
      'VIGENTE',
      'ID_INTERNACIONAL',
      'RAZON_SOCIAL_INSTITUCION',
      'DIRECCION_INSTITUCION',
      'TELEFONO_INSTITUCION',
      'EMAIL_INSTITUCION',
      'RUT_INSTITUCION',
      'LINK_INSTITUCION'
    );

    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'ID_INSTITUCION',
      'ID_TIPO_INSTITUCION',
      'ID_PAIS',
      'ID_INTERNACIONAL',
      'RUT_INSTITUCION',
      'NOMBRE_INSTITUCION',
      'VIGENTE',
      'RAZON_SOCIAL_INSTITUCION',
      'DIRECCION_INSTITUCION',
      'TELEFONO_INSTITUCION',
      'EMAIL_INSTITUCION',
      'LINK_INSTITUCION'
    );

        /* Aqui se puede modificar el nombre de los campos en la pantalla */
    $crud->display_as('ID_INSTITUCION','ID')
         ->display_as('ID_TIPO_INSTITUCION','ID Tipo')
         ->display_as('ID_PAIS','ID Pais')
         ->display_as('ID_INTERNACIONAL','ID Internacional')
         ->display_as('RUT_INSTITUCION','RUT Institución')
         ->display_as('NOMBRE_INSTITUCION','Nombre')
         ->display_as('RAZON_SOCIAL_INSTITUCION','Razón Social')
         ->display_as('DIRECCION_INSTITUCION','Dirección')
         ->display_as('TELEFONO_INSTITUCION','Teléfono')
         ->display_as('EMAIL_INSTITUCION','Correo')
         ->display_as('LINK_INSTITUCION','Website')
         ->display_as('VIGENTE','Vigente');


    /* Generamos la tabla */
    $output = $crud->render();

    /* La cargamos en la vista situada en
    /applications/views/institucion/administracion.php */
    $this->load->view('header', $output);
    $this->load->view('sidebar');
    $this->load->view('institucion/administracion', $output);
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  }

  function rut_check($rut)
    {
        $this->form_validation->set_message('rut_check', 'El campo {field} es inválido');
        if ($rut == ''){
            $this->form_validation->set_message('rut_check', 'El campo {field} es obligatorio');
            return FALSE;
        }
        else
        {
            $rut= str_replace('.', '', $rut); 
            if(strlen($rut) > 10) 
            { 
                return false; 
            } 
            
            if(strstr($rut, '-') == false) 
            { 
                return false; 
            } 
            
            $array_rut_sin_guion = explode('-',$rut); // separamos el la cadena del digito verificador. 
            $rut_sin_guion = $array_rut_sin_guion[0]; // la primera cadena 
            $digito_verificador = $array_rut_sin_guion[1];// el digito despues del guion. 


            if(is_numeric($rut_sin_guion)== false) 
            { 
                return false; 
            } 
            if ($digito_verificador != 'k' and $digito_verificador != 'K') 
            { 
                if(is_numeric($digito_verificador)== false)  
                  { 
                    return false; 
                  } 
            } 
            $cantidad = strlen($rut_sin_guion); //8 o 7 elementos 
            for ( $i = 0; $i < $cantidad; $i++)//pasamos el rut sin guion a un vector 
            { 
                $rut_array[$i] = $rut_sin_guion{$i}; 
            }   

            $i = ($cantidad-1); 
            $x=$i; 
            for ($ib = 0; $ib < $cantidad; $ib++)// ingresamos los elementos del ventor rut_array en otro vector pero al reves. 
            { 
                $rut_reverse[$ib]= $rut_array[$i];      
                $rut_reverse[$ib]; 
                $i=$i-1; 
            } 
                 
            $i=2; 
            $ib=0; 
            $acum=0;  
            do 
            { 
                if( $i > 7 ) 
                { 
                    $i=2; 
                } 
            $acum = $acum + ($rut_reverse[$ib]*$i); 
            $i++; 
            $ib++; 
            }while ( $ib <= $x); 

            $resto = $acum%11; 
            $resultado = 11-$resto; 
            if ($resultado == 11) { $resultado=0; } 
            if ($resultado == 10) { $resultado='k'; } 
            if ($digito_verificador == 'k' or $digito_verificador =='K') { $digito_verificador='k';} 

            if ($resultado == $digito_verificador) 
            { 
                return true; 
            } 
            else 
            { 
                return false; 
            } 

        }
    }

}