<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/* Heredamos de la clase Private_Controller */
class Modalidad_convenio extends Private_Controller {
 
  function __construct()
  {
 
    parent::__construct();
 
    /* Cargamos la base de datos */
    $this->load->database();
 
    /* Cargamos la libreria*/
    $this->load->library('Grocery_CRUD');
 
    /* Añadimos el helper al controlador */
    $this->load->helper('url');
  }
 
  function index()
  {
    /*
     * Mandamos todo lo que llegue a la funcion
     * administracion().
     **/
    if(!@$this->user) redirect ('welcome/login');

    redirect('modalidad_convenio/administracion');
  }
 
  /*
   *
   **/
  function administracion()
  {
    if(!@$this->user) redirect ('welcome/login');

    try{
 
    /* Creamos el objeto */
    $crud = new grocery_CRUD();
 
 
    /* Seleccionmos el nombre de la tabla de nuestra base de datos*/
    $crud->set_table('cnv_modalidad_convenio');
 
    /* Le asignamos un nombre */
    $crud->set_subject('Administración de Modalidad Convenios');
 
    /* Asignamos el idioma español */
    $crud->set_language('spanish');
 
    /* Aqui le decimos a grocery que estos campos son obligatorios */
    $crud->required_fields(
      'ID_MODALIDAD_CONVENIO',
      'ID_CONVENIO',
      'NOMBRE_MODALIDAD_CONVENIO',
      'DESCRIPCION',
      'VIGENTE'
    );
 
    /* Aqui le indicamos que campos deseamos mostrar */
    $crud->columns(
      'ID_MODALIDAD_CONVENIO',
      'ID_CONVENIO',
      'NOMBRE_MODALIDAD_CONVENIO',
      'DESCRIPCION',
      'VIGENTE'
    );
    $crud->set_relation('ID_CONVENIO','cnv_convenio','NOMBRE_CONVENIO');

    $crud->display_as('ID_MODALIDAD_CONVENIO','ID')
         ->display_as('ID_CONVENIO', 'Convenio')
         ->display_as('NOMBRE_MODALIDAD_CONVENIO','Nombre Modalidad Convenio')
         ->display_as('DESCRIPCION','Descripción')
         ->display_as('VIGENTE','Vigente');


    /* Generamos la tabla */
    $output = $crud->render();
 
    /* La cargamos en la vista situada en
    /applications/views/productos/administracion.php */
    
    $this->load->view('header', $output);
    $this->load->view('sidebar');
    $this->load->view('modalidad_convenio/administracion', $output);
 
    }catch(Exception $e){
      /* Si algo sale mal cachamos el error y lo mostramos */
      show_error($e->getMessage().' --- '.$e->getTraceAsString());
    }
  }
}