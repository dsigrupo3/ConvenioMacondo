<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Sistema de Gestión de convenios</title>
		<?php foreach($css_files as $file): ?>
		<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
		<?php endforeach; ?>
		<?php foreach($js_files as $file): ?>
		<script src="<?php echo $file; ?>"></script>
		<?php endforeach; ?>
		<link href="<?php echo base_url("assets/css/header.css");?>" rel="stylesheet" type="text/css" />
	</head>
	<body>
	<div class="menu">
	    <div class="container-fluid">
			<div class="navbar-header">
				<span class="glyphicon glyphicon-education"></span>
				<a>Universidad de Macondo</a>
			</div>
			<div>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="<?php echo site_url('welcome/logout'); ?>" class="logout"><span class="glyphicon glyphicon-log-out"></span> Cerrar Sesión</a></li>
				</ul>
			</div>
		</div>
	</div>
