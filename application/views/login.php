<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Inicio de Sesión</title>
		<link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url("assets/css/bootstrap-theme.min.css");?>" rel="stylesheet" type="text/css" />    
		<script src="<?php echo base_url("assets/js/jquery-3.1.1.min.js");?>"></script>    
		<script src="<?php echo base_url("assets/js/bootstrap.min.js");?>"></script>
		<link href="<?php echo base_url("assets/css/login.css");?>" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<?php if(@$error_login): ?> 
			<script>
				alert("Error en el usuario o contraseña.");
			</script>
		<?php endif; ?>

		<?php echo @validation_errors(); ?>

		<br />

		<div class = "container">
			<div class="wrapper">
				<form action="" method="post" name="Login_Form" class="form-signin">       
					<h3 class="form-signin-heading">¡Bienvenido! Por favor, inicie sesión</h3>
					  <hr class="colorgraph"><br>
					  
					  <input type="text" class="form-control" name="username" placeholder="Usuario" required="" autofocus="" value="<?php echo @$_POST['username']; ?>"/>
					  <input type="password" class="form-control" name="password" placeholder="Contraseña" required="" value="<?php echo @$_POST['password']; ?>"/>     		  
					 
					  <button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Login" type="Submit">Iniciar Sesión</button>  			
				</form>			
			</div>
		</div>
	</body>
</html>