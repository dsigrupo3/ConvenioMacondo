/* Copyright (c) 2009 Jos� Joaqu�n N��ez (josejnv@gmail.com) http://joaquinnunez.cl/blog/
 * Licensed under GPL (http://www.opensource.org/licenses/gpl-2.0.php)
 * Use only for non-commercial usage.
 *
 * Version : 0.5
 *
 * Requires: jQuery 1.2+
 */
 
(function($)
{
  jQuery.fn.Rut = function(options)
  {
    var defaults = {
      format: true,
      format_on: 'change'
    };

    var opts = $.extend(defaults, options);

    return this.each(function(){
    
      if(defaults.format)
      {
        jQuery(this).bind(defaults.format_on, function(){
          jQuery(this).val(jQuery.Rut.formatear(jQuery(this).val(),defaults.digito_verificador==null));
        });
      }
      if(defaults.validation)
      {
        if(defaults.digito_verificador == null)
        {
          jQuery(this).bind('blur', function(){
            var rut = jQuery(this).val();
            if(jQuery(this).val() != "" && !jQuery.Rut.validar(rut))
            {
                defaults.on_error();
            }
            else if(jQuery(this).val() != "")
            {
                defaults.on_success();
            }
          });
        }
        else
        {
          var id = jQuery(this).attr("id");
          jQuery(defaults.digito_verificador).bind('blur', function(){
            var rut = jQuery("#"+id).val()+"-"+jQuery(this).val();
            if(jQuery(this).val() != "" && !jQuery.Rut.validar(rut))
            {
                defaults.on_error();
            }
            else if(jQuery(this).val() != "")
            {
                defaults.on_success();
            }
          });
        }
      }
    });
  }
})(jQuery);

/**
  Funciones
*/


jQuery.Rut = {

  formatear:  function(Rut, digitoVerificador)
          {
          var sRut = new String(Rut);
          var sRutFormateado = '';
          sRut = jQuery.Rut.quitarFormato(sRut);
          if(digitoVerificador){
            var sDV = sRut.charAt(sRut.length-1);
            sRut = sRut.substring(0, sRut.length-1);
          }
          while( sRut.length > 3 )
          {
            sRutFormateado = "." + sRut.substr(sRut.length - 3) + sRutFormateado;
            sRut = sRut.substring(0, sRut.length - 3);
          }
          sRutFormateado = sRut + sRutFormateado;
          if(sRutFormateado != "" && digitoVerificador)
          {
            sRutFormateado += "-"+sDV;
          }
          else if(digitoVerificador)
          {
            sRutFormateado += sDV;
          }
          
          return sRutFormateado;
        },

  quitarFormato: function(rut)
        {
          var strRut = new String(rut);
          while( strRut.indexOf(".") != -1 )
          {
          strRut = strRut.replace(".","");
          }
          while( strRut.indexOf("-") != -1 )
          {
          strRut = strRut.replace("-","");
          }
          
          return strRut;
        }

};


function funrut(){
    $('#field-RUT_INSTITUCION').Rut({
    format_on: 'keyup'
    });
    $('#field-RUT_COORDINADOR_CONVENIO').Rut({
    format_on: 'keyup'
    });
}

$(document).ready(funrut);